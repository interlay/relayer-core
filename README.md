# Relayer Core

Core logic for reading `Backing` block headers and submitting them to an `Issuing` client.